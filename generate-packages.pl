#!/usr/bin/env perl

=begin
generate-packages.pl parses a yaml to file to generate tarballs to push into GitLab's package registry

Copyright (C) 2023 Alfred Wingate

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=cut

use autodie;
use feature 'signatures';
use strict;
use warnings;

no warnings qw(experimental::signatures);

use Cwd qw(getcwd);
use File::Slurp qw(read_file);
use File::Temp qw(tempdir);
use File::Path qw(rmtree);
use Getopt::Long qw(GetOptions);
use Module::Load qw(load);
use JSON::Parse 'parse_json';
use YAML::Tiny;

use lib getcwd() . '/helpers';

load WWWHelper, qw(raw_request);

sub gitlab_api_request ( $action, $api_info, $url_end, $content = 0 ) {
    my $url =
      "${$api_info}{'url'}/projects/${$api_info}{'project_id'}/${url_end}";
    my $header = {
        ${$api_info}{'ci'}
        ? 'JOB-TOKEN'
        : 'PRIVATE-TOKEN' => ${$api_info}{'token'}
    };
    return $content
      ? raw_request( $action, $url, 'header', $header, 'content', $content )
      : raw_request( $action, $url, 'header', $header );
}

sub is_uploaded ( $list, $package ) {
    foreach ( @{$list} ) {
        if (   ${$package}{'name'} eq ${$_}{'name'}
            && ${$package}{'version'} eq ${$_}{'version'} )
        {
            return 1;
        }
    }
    return 0;
}

sub list_packages ($api_info) {
    my $response = gitlab_api_request( 'GET', $api_info, 'packages' );

    if ( $response->is_error ) {
        die 'Couldn\'t get list of packages: ' . $response->status_line . "\n";
    }
    return parse_json( $response->content );
}

sub upload_package ( $tarball, $package, $api_info ) {
    print
      "Uploading ${tarball} for ${$package}{'name'}-${$package}{'version'}\n";

    my $file = read_file( $tarball, { binmode => ':raw' } );

    use File::Basename;

    my $response = gitlab_api_request(
        'PUT',
        $api_info,
"packages/generic/${$package}{'name'}/${$package}{'version'}/${\(basename($tarball))}",
        $file
    );

    if ( !$response->is_success ) {
        die "Couldn\'t upload ${tarball}: " . $response->status_line . "\n";
    }

    return 1;
}

sub delete_package ( $package, $api_info ) {
    print
"Deleting ${$package}{'name'}-${$package}{'version'} with id ${$package}{'id'}\n";

    my $response =
      gitlab_api_request( 'DELETE', $api_info, "packages/${$package}{'id'}" );

    if ( !$response->is_success ) {
        die "Couldn\'t delete ${$package}{'id'}: "
          . $response->status_line . "\n";
    }

    return 1;
}

sub parse_config ($file_name) {
    my $config = YAML::Tiny->read($file_name);
    return $config->[0];
}

sub cleanup_packages ( $list, $package_list, $api_info ) {
    foreach ( @{$list} ) {
        next if is_uploaded( $package_list, $_ );

        delete_package( $_, $api_info );
    }
    return 1;
}

sub main() {
    my $ci             = undef;
    my $cleanup        = undef;
    my $config         = 'package-list.yml';
    my $gitlab_api_url = 'https://gitlab.com/api/v4';
    my $token          = undef;
    my $project_id     = undef;

    GetOptions(
        'ci'               => \$ci,
        'cleanup'          => \$cleanup,
        'config=s'         => \$config,
        'gitlab_api_url=s' => \$gitlab_api_url,
        'token=s'          => \$token,
        'project_id=i'     => \$project_id,
    ) or die "Error in command line arguments\n";

    if ( !$token || !$project_id ) {
        die "Mandatory --token or --project_id unset\n";
    }

    $gitlab_api_url =~ s/\/$//sxm;

    my %api_info = (
        ci         => $ci,
        url        => $gitlab_api_url,
        token      => $token,
        project_id => $project_id,
    );

    my $parsed_config = parse_config($config);

    my $tempdir  = tempdir();
    my $startdir = getcwd();

    use lib getcwd() . '/modules';
    my $list = list_packages( \%api_info );

    my $package_list;

    for my $key ( keys %{$parsed_config} ) {
        load $key, qw(create_package tarball_name);
        for my $package_entry ( @{ $parsed_config->{$key} } ) {
            for my $version ( @{ $package_entry->{'versions'} } ) {
                my %package = %{$package_entry};
                delete $package{'versions'};
                $package{'version'} = $version;

                push @{$package_list}, \%package;

                next if is_uploaded( $list, \%package );

                my $tarball = create_package( $tempdir, \%package );

                upload_package( "${tempdir}/${tarball}", \%package,
                    \%api_info );
            }
        }
    }

    # Ci token doesnt have authority to delete packages
    if ( $cleanup && !${api_info}{'ci'} ) {
        cleanup_packages( $list, $package_list, \%api_info );
    }

    chdir $startdir;
    rmtree($tempdir);
    return 1;
}

main();
