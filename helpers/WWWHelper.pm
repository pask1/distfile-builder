=begin
Helper functions for modules in ../modules

Copyright (C) 2023 Alfred Wingate

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=cut

package WWWHelper;
use autodie;
use feature 'signatures';
use strict;
use warnings;

no warnings qw(experimental::signatures);

use Exporter qw(import);
our @EXPORT_OK = qw(fetch_file raw_request);

sub fetch_file ( $url, $filename ) {
    my $response = raw_request( 'GET', $url, 'filename', $filename );
    if ( !$response->is_success ) {
        croak(
"Downloading ${filename} from ${url} failed with:\n$response->status_line\n"
        );
    }
    return 1;

}

sub raw_request ( $action, $url, %optionals ) {
    use LWP::UserAgent;
    my $useragent = LWP::UserAgent->new;

    use HTTP::Request;
    my $request = HTTP::Request->new( $action => $url );

    $optionals{'header'}  && $request->header( %{ $optionals{'header'} } );
    $optionals{'content'} && $request->content( $optionals{'content'} );

    return $optionals{'filename'}
      ? $useragent->request( $request, $optionals{'filename'} )
      : $useragent->request($request);
}

1;
