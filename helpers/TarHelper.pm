=begin
Helper functions for modules in ../modules

Copyright (C) 2023 Alfred Wingate

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=cut

package TarHelper;
use autodie;
use feature 'signatures';
use strict;
use warnings;

no warnings qw(experimental::signatures);

use Archive::Tar;

use Exporter qw(import);
our @EXPORT_OK = qw(create_tarball extract_tarball supported_compression);

sub supported_compression ( $constant = 0 ) {

    # prefer xz
    Archive::Tar->has_xz_support
      && $constant ? return COMPRESS_XZ : return 'xz';
    Archive::Tar->has_bzip2_support
      && $constant ? return COMPRESS_BZIP : return 'bz';
    Archive::Tar->has_zlib_support
      && $constant ? return COMPRESS_GZIP : return 'gz';

    die "No compression support\n";
}

sub create_tarball ( $tarball, $filelist ) {
    my $archive =
      Archive::Tar->create_archive( $tarball, supported_compression('1'),
        @{$filelist} );

    if ( !$archive ) {
        die "Couldn't create ${tarball}: " . Archive::Tar->error() . "\n";
    }

    return 1;
}

sub extract_tarball ($tarball) {
    Archive::Tar->can_handle_compressed_files
      || die "Can't handle compressed files\n";
    return Archive::Tar->extract_archive($tarball);
}

1;
