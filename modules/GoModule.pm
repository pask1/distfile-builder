=begin
Go module specifc instructions to be used by generate-packages.pl

Copyright (C) 2023 Alfred Wingate

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=cut

package GoModule;
use strict;
use warnings;
use autodie;
use feature 'signatures';

no warnings qw(experimental::signatures);

use Cwd qw(getcwd);
use Module::Load;

use lib getcwd() . 'helpers';

load TarHelper, qw(create_tarball extract_tarball supported_compression);

load WWWHelper, qw(fetch_file);

use Exporter 'import';
our @EXPORT_OK = qw(tarball_name create_package);

sub tarball_name ($package) {
    return
"${$package}{'name'}-${$package}{'version'}-deps.tar.${\(supported_compression())}";
}

sub create_package ( $tempdir, $package ) {
    my $name    = ${$package}{'name'};
    my $version = ${$package}{'version'};
    my $url     = ${$package}{'url'};

    print "Creating tarball for ${name}-${version}\n";

    # switch out required variables into the url
    $url =~ s/(\$[{]\w+[}])/$1/eegxsm;

    my $tarball = "${tempdir}/${\(tarball_name($package))}";

    my $packagedir = "${tempdir}/${name}-${version}";
    mkdir $packagedir;
    chdir $packagedir;

    # Assume github syntax
    my $download_tarball = "${name}-${version}.tar.gz";

    fetch_file( $url, $download_tarball );
    extract_tarball("${name}-${version}.tar.gz");

    # Naively find the dir so that I dont have to care about the name
    my $extractdir;
    foreach ( glob "${packagedir}/*" ) {
        if (-d) {
            $extractdir = $_;
            last;
        }
    }

    if ( !$extractdir ) {
        croak('finding extracted dir failed');
    }

    chdir $extractdir;

    local $ENV{'GOMODCACHE'} = "${extractdir}/go-mod";

    system 'go mod download -modcacherw';

    my @filelist = ();
    use File::Find;

    find( sub { push @filelist, $File::Find::name }, 'go-mod' );

    create_tarball( $tarball, \@filelist );

    return tarball_name($package);
}

1;
